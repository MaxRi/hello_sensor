package com.example.hellosensor;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import androidx.appcompat.app.AppCompatActivity;


public class MainActivity extends AppCompatActivity {
    //public static final String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";
    private Button compButton;
    private Button accelButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        compButton = (Button) findViewById(R.id.compassButton);
        accelButton = (Button) findViewById(R.id.accelButton);


        compButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getCompass();
            }
        });

        accelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getAccel();
            }
        });
    }
    public void getCompass(){
        Intent compIntent = new Intent (this, CompassActivity.class);
        startActivity(compIntent);
    }

    public void getAccel(){
        Intent accelIntent = new Intent (this, AccelerometerActivity.class);
        startActivity(accelIntent);
    }

}

